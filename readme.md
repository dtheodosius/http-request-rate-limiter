# Rate Limiter
This library is written to support any Java Http Server that supports _javax.servlet.http._ packages. An example of the Http Server is **Jetty**.

It contains three main components:

- *HttpRequestRateLimiter* that will apply a particular strategy to check if a request is valid or not, i.e. a request does not exceed the limit per specified period. For example when there is a limit of 100 requests/hour, the 101st request will be rejected.
- *HttpRequestRateLimiterStrategy* that checks number of requests from the requester for a particular time period from a particular cache. There is an implementation called `HourlyHttpRequestRateLimiterStrategy` that constraint the period to be 1 hour.
- *HttpRequestRateLimiterCache* that provides a mechanism to store and retrieve the requests from the requester. The implementation `InMemoryHttpRequestRateLimiterCache` provides a quick way to maintain the requests but not recommended to use if you want to have multiple JVM processes running. `PostgresqlHttpRequestRateLimiterCache` is a more recommended cache to use when you have multiple JVM processes running.

You can choose a combination of the strategy and cache accordingly. You can add custom implementation of the **Strategy** by extending it. Also you can add you own cache, such as *Redis* by implement the interface.

## Usage
To initialise a limiter:
```
HttpRequestRateLimiterCache cache = new InMemoryHttpRequestRateLimiterCache();

HttpRequestRateLimiterStrategy strategy = new HourlyHttpRequestRateLimiterStrategy(cache, 100);

HttpRequestRateLimiter limiter = new HttpRequestRateLimiter(strategy);
```
Example in Jetty:
```
public class HelloWorld extends AbstractHandler
{
    private final HttpRequestRateLimiter limiter;

	public HelloWorld(HttpRequestRateLimiter limiter) {
        this.limiter = limiter;
    }
    public void handle(String target,
                       Request baseRequest,
                       HttpServletRequest request,
                       HttpServletResponse response)
        throws IOException, ServletException
    {
		limiter.handle(
		    request,
		    response,
		    (req, resp) -> {
                resp.setContentType("text/html;charset=utf-8");
                resp.setStatus(HttpServletResponse.SC_OK);
                baseRequest.setHandled(true);
                resp.getWriter().println("<h1>Hello World</h1>");

            }
    }
 }
```

## Requirement
You need to have:

 - **Java 8** installed
 - **Docker** installed

## How to build it?
You can execute
> ./go

What it does is it will spin up **postgresql** in **Docker** and then run the tests
## How to publish it?
Currently it has been only tested using local maven:
> ./gradlew install #will publish to local maven

Once it is published, you can use it from your machine using **maven**:
```
<!-- https://mvnrepository.com/artifact/org.mockito/mockito-core -->
<dependency>
    <groupId>com.au.dtheodosius</groupId>
    <artifactId>rate-limiter</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```
 or **gradle** by executing
`compile 'com.au.dtheodosius:rate-limiter:1.0-SNAPSHOT'`

