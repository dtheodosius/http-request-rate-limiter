#!/bin/bash

docker-compose up postgres&

until psql -h localhost -U "postgres" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

./gradlew clean test

docker-compose down
