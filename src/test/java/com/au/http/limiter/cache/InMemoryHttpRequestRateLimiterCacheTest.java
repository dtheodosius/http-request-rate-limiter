package com.au.http.limiter.cache;

import com.au.http.limiter.HttpRequestRateLimiterCache;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InMemoryHttpRequestRateLimiterCacheTest {

  @Test
  public void shouldAddRequestCorrectly() {
    // given
    ConcurrentHashMap<String, List<LocalDateTime>> map = new ConcurrentHashMap<>();
    HttpRequestRateLimiterCache cache = new InMemoryHttpRequestRateLimiterCache(map);
    LocalDateTime now = LocalDateTime.now();

    // when
    cache.add("1.2.3.4", now);

    // then
    assertTrue(map.containsKey("1.2.3.4"));
    assertEquals(now, map.get("1.2.3.4").get(0));
  }

  @Test
  public void shouldAddConcurrentRequestCorrectly() throws InterruptedException {
    // given
    ConcurrentHashMap<String, List<LocalDateTime>> map = new ConcurrentHashMap<>();
    HttpRequestRateLimiterCache cache = new InMemoryHttpRequestRateLimiterCache(map);
    LocalDateTime now = LocalDateTime.now();

    // when
    Runnable runnable1 = () -> cache.add("1.2.3.4", now);
    Runnable runnable2 = () -> cache.add("1.2.3.4", now.plusMinutes(5));

    // when
    ExecutorService executor = Executors.newFixedThreadPool(2);
    executor.submit(runnable1);
    executor.submit(runnable2);

    Thread.sleep(1000);

    // then
    assertTrue(map.containsKey("1.2.3.4"));
    assertEquals(2, map.get("1.2.3.4").size());
  }

  @Test
  public void shouldReturnRequestCountWithInTimeFrame() {
    // given
    ConcurrentHashMap<String, List<LocalDateTime>> map = new ConcurrentHashMap<>();
    HttpRequestRateLimiterCache cache = new InMemoryHttpRequestRateLimiterCache(map);
    LocalDateTime now = LocalDateTime.now();
    cache.add("1.2.3.4", now);

    // when
    int result = cache.getRequestCount("1.2.3.4", now.minusHours(1), now.plusHours(1));

    // then
    assertEquals(1, result);
  }

  @Test
  public void shouldReturnZeroRequestCount_whenNoRemoteAddress() {
    // given
    ConcurrentHashMap<String, List<LocalDateTime>> map = new ConcurrentHashMap<>();
    HttpRequestRateLimiterCache cache = new InMemoryHttpRequestRateLimiterCache(map);
    LocalDateTime now = LocalDateTime.now();
    cache.add("1.2.3.4", now);

    // when
    int result = cache.getRequestCount("1.2.3.5", now.minusHours(1), now.plusHours(1));

    // then
    assertEquals(0, result);
  }

  @Test
  public void shouldReturnZeroRequestCount_whenNotInTimeFrame() {
    // given
    ConcurrentHashMap<String, List<LocalDateTime>> map = new ConcurrentHashMap<>();
    HttpRequestRateLimiterCache cache = new InMemoryHttpRequestRateLimiterCache(map);
    LocalDateTime now = LocalDateTime.now();
    cache.add("1.2.3.4", now);

    // when
    int result = cache.getRequestCount("1.2.3.4", now.plusHours(1), now.plusHours(2));

    // then
    assertEquals(0, result);
  }
}