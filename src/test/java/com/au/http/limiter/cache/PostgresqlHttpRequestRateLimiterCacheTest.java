package com.au.http.limiter.cache;

import com.au.http.limiter.HttpRequestRateLimiterCache;
import com.au.http.limiter.sql.ConnectionPool;
import com.au.http.limiter.sql.PgHikariConnectionPool;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;

public class PostgresqlHttpRequestRateLimiterCacheTest {

  @Test
  public void shouldAddRequestCorrectly() throws SQLException {
    // given
    PgHikariConnectionPool connectionPool = new PgHikariConnectionPool(
      "localhost",
      "postgres",
      "postgres",
      "sa",
      10
    );
    HttpRequestRateLimiterCache cache = new PostgresqlHttpRequestRateLimiterCache(connectionPool);
    LocalDateTime now = LocalDateTime.now();

    // when
    cache.add("1.2.3.4", now);

    // then
    assertEquals(1, count("1.2.3.4", connectionPool));
    truncate(connectionPool);
    connectionPool.close();
  }

  @Test
  public void shouldAddConcurrentRequestCorrectly() throws InterruptedException, SQLException {
    // given
    PgHikariConnectionPool connectionPool = new PgHikariConnectionPool(
      "localhost",
      "postgres",
      "postgres",
      "sa",
      10
    );
    HttpRequestRateLimiterCache cache = new PostgresqlHttpRequestRateLimiterCache(connectionPool);
    LocalDateTime now = LocalDateTime.now();
    // when
    Runnable runnable1 = () -> cache.add("1.2.3.4", now);
    Runnable runnable2 = () -> cache.add("1.2.3.4", now.plusMinutes(5));

    // when
    ExecutorService executor = Executors.newFixedThreadPool(2);
    executor.submit(runnable1);
    executor.submit(runnable2);

    Thread.sleep(1000);

    // then
    assertEquals(2, count("1.2.3.4", connectionPool));
    truncate(connectionPool);
    connectionPool.close();
  }

  @Test
  public void shouldReturnRequestCountWithInTimeFrame() throws SQLException {
    // given
    PgHikariConnectionPool connectionPool = new PgHikariConnectionPool(
      "localhost",
      "postgres",
      "postgres",
      "sa",
      10
    );
    HttpRequestRateLimiterCache cache = new PostgresqlHttpRequestRateLimiterCache(connectionPool);
    LocalDateTime now = LocalDateTime.now();
    cache.add("1.2.3.4", now);
    cache.add("1.2.3.4", now.plusMinutes(40));

    // when
    int result = cache.getRequestCount("1.2.3.4", now.minusHours(1), now.plusHours(1));

    // then
    truncate(connectionPool);
    assertEquals(2, result);
    connectionPool.close();
  }

  @Test
  public void shouldReturnZeroRequestCount_whenNoRemoteAddress() throws SQLException {
    // given
    PgHikariConnectionPool connectionPool = new PgHikariConnectionPool(
      "localhost",
      "postgres",
      "postgres",
      "sa",
      10
    );
    HttpRequestRateLimiterCache cache = new PostgresqlHttpRequestRateLimiterCache(connectionPool);
    LocalDateTime now = LocalDateTime.now();
    cache.add("1.2.3.4", now);
    cache.add("1.2.3.4", now.plusMinutes(40));

    // when
    int result = cache.getRequestCount("1.2.3.5", now.minusHours(1), now.plusHours(1));

    // then
    truncate(connectionPool);
    assertEquals(0, result);
    connectionPool.close();
  }

  @Test
  public void shouldReturnZeroRequestCount_whenNotInTimeFrame() throws SQLException {
    // given
    PgHikariConnectionPool connectionPool = new PgHikariConnectionPool(
      "localhost",
      "postgres",
      "postgres",
      "sa",
      10
    );
    HttpRequestRateLimiterCache cache = new PostgresqlHttpRequestRateLimiterCache(connectionPool);
    LocalDateTime now = LocalDateTime.now();
    cache.add("1.2.3.4", now);
    cache.add("1.2.3.4", now.plusMinutes(40));

    // when
    int result = cache.getRequestCount("1.2.3.4", now.plusHours(1), now.plusHours(2));

    // then
    truncate(connectionPool);
    assertEquals(0, result);
    connectionPool.close();
  }

  private void truncate(ConnectionPool pool) {
    Connection connection = pool.getConnection();
    try {
      PreparedStatement preparedStatement = connection.prepareStatement("TRUNCATE rate_limiter");
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
    }
  }

  private int count(String remoteAddress, ConnectionPool pool) {
    Connection connection = pool.getConnection();
    try {
      PreparedStatement preparedStatement = connection.prepareStatement("SELECT count(1) FROM rate_limiter WHERE remote_address = ?");
      preparedStatement.setString(1, remoteAddress);

      ResultSet resultSet = preparedStatement.executeQuery();
      resultSet.next();
      return resultSet.getInt(1);
    } catch (SQLException e) {
      return Integer.MAX_VALUE;
    }
  }
}