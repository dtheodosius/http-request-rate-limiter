package com.au.http.limiter;

import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class HttpRequestRateLimiterStrategyTest {

  private String remoteAddress = "1.2.3.4";
  private HttpServletRequest request;
  private HttpRequestRateLimiterCache cache;

  @Before
  public void setup() {
    request = mock(HttpServletRequest.class);
    cache = mock(HttpRequestRateLimiterCache.class);
    when(request.getRemoteAddr()).thenReturn(remoteAddress);
  }

  @Test
  public void shouldAllowRequestNotExceedingTheMaximumRequestNumber() {
    // given
    when(cache.getRequestCount(eq(remoteAddress), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(1);
    HttpRequestRateLimiterStrategy strategy = getTestStrategy(cache, 10);

    // when
    boolean result = strategy.allowRequest(request);

    // then
    assertTrue(result);
  }

  @Test
  public void shouldAddRequestToCacheWhenRequestNotExceedingTheMaximumRequestNumber() {
    // given
    when(cache.getRequestCount(eq(remoteAddress), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(1);
    HttpRequestRateLimiterStrategy strategy = getTestStrategy(cache, 10);

    // when
    strategy.allowRequest(request);

    // then
    verify(cache).add(eq(remoteAddress), any(LocalDateTime.class));
  }

  @Test
  public void shouldRejectRequestExceedingTheMaximumRequestNumber() {
    // given
    when(cache.getRequestCount(eq(remoteAddress), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(1);
    HttpRequestRateLimiterStrategy strategy = getTestStrategy(cache, 1);

    // when
    boolean result = strategy.allowRequest(request);

    // then
    assertFalse(result);
  }

  @Test
  public void shouldNotAddRequestToCacheWhenRequestExceedingTheMaximumRequestNumber() {
    // given
    when(cache.getRequestCount(eq(remoteAddress), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(1);
    HttpRequestRateLimiterStrategy strategy = getTestStrategy(cache, 1);

    // when
    strategy.allowRequest(request);

    // then
    verify(cache, times(0)).add(eq(remoteAddress), any(LocalDateTime.class));
  }

  private HttpRequestRateLimiterStrategy getTestStrategy(
    HttpRequestRateLimiterCache cache,
    int numberOfMaximumRequests
  ) {
    return new HttpRequestRateLimiterStrategy(cache, numberOfMaximumRequests) {
      @Override
      public Duration getDuration() {
        return Duration.ofHours(1);
      }
    };
  }
}