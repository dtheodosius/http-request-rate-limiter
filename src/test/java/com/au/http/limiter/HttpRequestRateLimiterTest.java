package com.au.http.limiter;

import com.au.http.limiter.HttpRequestRateLimiter.HttpRequestRateLimiterProcessor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class HttpRequestRateLimiterTest {

  private HttpServletRequest request;
  private HttpServletResponse response;
  private HttpRequestRateLimiterStrategy strategy;

  @Before
  public void setup() {
    request = mock(HttpServletRequest.class);
    response = mock(HttpServletResponse.class);

    strategy = mock(HttpRequestRateLimiterStrategy.class);
  }

  @Test
  public void shouldReturnForbiddenWhenRequestIsNotAllowed() {
    // given
    ArgumentCaptor<Integer> argument = ArgumentCaptor.forClass(Integer.class);
    when(strategy.allowRequest(request)).thenReturn(false);

    HttpRequestRateLimiter limiter = new HttpRequestRateLimiter(strategy);

    // when
    limiter.handle(request, response, (request1, response1) -> {
    });

    // then
    verify(response).setStatus(argument.capture());
    assertEquals(HttpServletResponse.SC_FORBIDDEN, (int) argument.getValue());
  }

  @Test
  public void shouldProcessTheRequestWhenRequestIsAllowed() {
    // given
    HttpRequestRateLimiterProcessor processor = mock(HttpRequestRateLimiterProcessor.class);
    when(strategy.allowRequest(request)).thenReturn(true);

    HttpRequestRateLimiter limiter = new HttpRequestRateLimiter(strategy);

    // when
    limiter.handle(request, response, processor);

    // then
    verify(processor).process(request, response);
  }
}