package com.au.http.limiter.cache;

import com.au.http.limiter.HttpRequestRateLimiterCache;
import com.au.http.limiter.liquibase.LiquibaseMigration;
import com.au.http.limiter.sql.PgHikariConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Singleton
public class PostgresqlHttpRequestRateLimiterCache implements HttpRequestRateLimiterCache {

  private static Logger log = LoggerFactory.getLogger(PostgresqlHttpRequestRateLimiterCache.class);
  private PgHikariConnectionPool connectionPool;

  private final String getRequestCountSql = "SELECT count(1) FROM rate_limiter WHERE remote_address = ? " +
    "AND request_timestamp >= ? AND request_timestamp <= ?";
  private final String insertRequestSql= "INSERT INTO rate_limiter (remote_address, request_timestamp) VALUES (?, ?)";

  public PostgresqlHttpRequestRateLimiterCache(PgHikariConnectionPool connectionPool) {
    this.connectionPool = connectionPool;
    LiquibaseMigration.runLiquibaseUpgrade(new String[]{}, connectionPool.getConnection());
  }

  @Override
  public int getRequestCount(String remoteAddress, LocalDateTime from, LocalDateTime to) {
    Connection connection = connectionPool.getConnection();
    try {
      PreparedStatement preparedStatement = connection.prepareStatement(getRequestCountSql);
      preparedStatement.setString(1, remoteAddress);
      preparedStatement.setTimestamp(2, Timestamp.valueOf(from));
      preparedStatement.setTimestamp(3, Timestamp.valueOf(to));

      ResultSet resultSet = preparedStatement.executeQuery();
      resultSet.next();
      return resultSet.getInt(1);
    } catch (SQLException e) {
      return Integer.MAX_VALUE;
    }
  }

  @Override
  public void add(String remoteAddress, LocalDateTime requestDateTime) {
    Connection connection = connectionPool.getConnection();
    try {
      PreparedStatement preparedStatement = connection.prepareStatement(insertRequestSql);
      preparedStatement.setString(1, remoteAddress);
      preparedStatement.setTimestamp(2, Timestamp.valueOf(requestDateTime));

      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      log.error("unable to add request to the database");
    }
  }
}
