package com.au.http.limiter.cache;

import com.au.http.limiter.HttpRequestRateLimiterCache;

import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class InMemoryHttpRequestRateLimiterCache implements HttpRequestRateLimiterCache {

  private Map<String, List<LocalDateTime>> requests;

  public InMemoryHttpRequestRateLimiterCache() {
    this(new ConcurrentHashMap<>());
  }

  InMemoryHttpRequestRateLimiterCache(Map<String, List<LocalDateTime>> requests) {
    this.requests = requests;
  }

  @Override
  public int getRequestCount(String remoteAddress, LocalDateTime from, LocalDateTime to) {
    return (int) requests.getOrDefault(remoteAddress, new ArrayList<>()).stream()
      .filter(
        localDateTime ->
          !localDateTime.isBefore(from) && !localDateTime.isAfter(to)
      )
      .count();
  }

  @Override
  public void add(String remoteAddress, LocalDateTime requestDateTime) {
    List<LocalDateTime> requestDateTimes = requests.getOrDefault(remoteAddress, new ArrayList<>());
    requestDateTimes.add(requestDateTime);
    requests.put(remoteAddress, requestDateTimes);
  }
}
