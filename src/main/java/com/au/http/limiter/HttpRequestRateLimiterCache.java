package com.au.http.limiter;

import java.time.LocalDateTime;

/**
 * Defines a mechanism to store each of the request and ability to
 * get total count of requests from a particular remode address
 * in a specific period of time
 */
public interface HttpRequestRateLimiterCache {

  /**
   * Obtaining the total number of requests for a particular ip address
   * for a specified timeframe
   * @param remoteAddress the ip address of the requester
   * @param from the start {@link LocalDateTime} to filter
   * @param to the end {@link LocalDateTime} to filter
   * @return number of requests in the time frame
   */
  int getRequestCount(String remoteAddress, LocalDateTime from, LocalDateTime to);

  /**
   * Store request {@link LocalDateTime} for each remoteAddress
   * @param remoteAddress the ip address of the requester
   * @param requestDateTime the {@link LocalDateTime} of the request
   */
  void add(String remoteAddress, LocalDateTime requestDateTime);
}
