package com.au.http.limiter.strategy;

import com.au.http.limiter.HttpRequestRateLimiterCache;
import com.au.http.limiter.HttpRequestRateLimiterStrategy;

import java.time.Duration;

public class HourlyHttpRequestRateLimiterStrategy extends HttpRequestRateLimiterStrategy {

  public HourlyHttpRequestRateLimiterStrategy(HttpRequestRateLimiterCache cache, int numberOfMaximumRequests) {
    super(cache, numberOfMaximumRequests);
  }

  @Override
  public Duration getDuration() {
    return Duration.ofHours(1);
  }
}
