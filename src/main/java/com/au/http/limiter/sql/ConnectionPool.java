package com.au.http.limiter.sql;

import java.sql.Connection;

public interface ConnectionPool {
  Connection getConnection();

  void close();
}
