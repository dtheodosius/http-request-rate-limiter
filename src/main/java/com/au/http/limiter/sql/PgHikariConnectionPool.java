package com.au.http.limiter.sql;

import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class PgHikariConnectionPool implements ConnectionPool{
  private static Logger log = LoggerFactory.getLogger(PgHikariConnectionPool.class);
  private final HikariDataSource dataSource;

  public PgHikariConnectionPool(String host, String database, String username, String password, int maxConnections) throws SQLException {
    try {
      Class.forName("org.postgresql.Driver");
    } catch (ClassNotFoundException e) {
      log.error("Error loading class for PostgreSQL JDBC driver", e);
      throw new RuntimeException(e);
    }
    dataSource = new HikariDataSource();
    dataSource.setJdbcUrl(String.format("jdbc:postgresql://%s/%s", host, database));
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    dataSource.setMaximumPoolSize(maxConnections);
  }

  @Override
  public Connection getConnection() {
    try {
      return dataSource.getConnection();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void close() {
    if (dataSource != null && !dataSource.isClosed()) {
      dataSource.close();
    }
  }
}
