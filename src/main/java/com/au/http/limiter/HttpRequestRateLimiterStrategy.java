package com.au.http.limiter;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * HttpRequestRateLimiterStrategy defines if a particular request is allowed or not
 * by obtaining the number of requests in a specified
 * duration from a specified {@link HttpRequestRateLimiterCache}.
 */
public abstract class HttpRequestRateLimiterStrategy {

  private final HttpRequestRateLimiterCache cache;
  private final int numberOfMaximumRequests;

  public HttpRequestRateLimiterStrategy(HttpRequestRateLimiterCache cache, int numberOfMaximumRequests) {
    this.cache = cache;
    this.numberOfMaximumRequests = numberOfMaximumRequests;
  }

  public abstract Duration getDuration();
  /**
   * Check if a request is allowed by getting the count from a specific cache.
   * <p></p>
   * It is allowed when the requester number of request does not exceed
   * the numberOfMaximumRequests in the specified duration
   * @param request {@link HttpServletRequest} sent by the client
   * @return boolean
   */
  synchronized boolean allowRequest(HttpServletRequest request) {
    LocalDateTime to = LocalDateTime.now();
    LocalDateTime from = to.minusSeconds(getDuration().getSeconds());
    String remoteAddress = request.getRemoteAddr();

    int remoteAddressRequestsCount = cache.getRequestCount(remoteAddress, from, to);
    if (remoteAddressRequestsCount < numberOfMaximumRequests) {
      cache.add(remoteAddress, to);
      return true;
    }
    return false;
  }
}
