package com.au.http.limiter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * HttpRequestRateLimiter applies a specified strategy to check if a request is allowed
 * before processing the request.
 */
public class HttpRequestRateLimiter {

  private final HttpRequestRateLimiterStrategy strategy;

  public HttpRequestRateLimiter(HttpRequestRateLimiterStrategy strategy) {
    this.strategy = strategy;
  }

  /**
   * A handling mechanism that will apply a strategy to understand if a request
   * is allowed or not.
   * <p></p>
   * If it is not allowed, it will return forbidden.
   * Otherwise it will call processor to apply the behaviour intended when
   * handling a valid request
   *
   * @param request   {@link HttpServletRequest} that is sent by the requester
   * @param response  {@link HttpServletResponse} that is sent to the requester
   * @param processor provides default behaviour for valid request
   */
  public void handle(
    HttpServletRequest request,
    HttpServletResponse response,
    HttpRequestRateLimiterProcessor processor
  ) {
    if (strategy.allowRequest(request)) {
      processor.process(request, response);
    } else {
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    }
  }

  /**
   * A class that enhances {@link HttpServletResponse} by applying the behaviour
   * from the {@link HttpServletRequest}
   */
  public interface HttpRequestRateLimiterProcessor {

    /**
     * Process {@link HttpServletRequest} and send back
     * {@link HttpServletResponse} to client
     *
     * @param request  {@link HttpServletRequest} that is sent by the requester
     * @param response {@link HttpServletResponse} that is sent to the requester
     */
    void process(HttpServletRequest request, HttpServletResponse response);
  }
}
