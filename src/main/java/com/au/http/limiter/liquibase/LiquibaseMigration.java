package com.au.http.limiter.liquibase;

import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class LiquibaseMigration {

  private static Logger log = LoggerFactory.getLogger(LiquibaseMigration.class);

  public static void runLiquibaseUpgrade(String[] updateFiles, Connection connection) {
    Database database = null;
    try {
      database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
      Liquibase liquibase = new Liquibase("migrations.xml", new ClassLoaderResourceAccessor(), database);
      liquibase.update(new Contexts());

      for (String updateFile : updateFiles) {
        Liquibase l = new Liquibase(updateFile, new ClassLoaderResourceAccessor(), database);
        l.update(new Contexts());
      }

    } catch (Exception e) {
      log.error("Error running upgrade", e);
      throw new RuntimeException(e);
    } finally {
      try {
        if (database != null) {
          database.close();
        }
      } catch (Exception e) {
        log.warn("Error closing connection", e);
      }
      closeQuietly(connection);
    }
  }

  private static void closeQuietly(Connection connection) {
    try {
      if (connection != null && !connection.isClosed()) {
        connection.close();
        log.debug("closed {}", connection);
      }
    } catch (SQLException e) {
      log.debug(e.getMessage(), e);
    } catch (RuntimeException e) {
      log.debug(e.getMessage(), e);
    }
  }
}
